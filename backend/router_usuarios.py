from fastapi import  APIRouter, FastAPI, Depends, HTTPException, status, Response

from  database import engine,SessionLocal, Base
from schema import ProdutosSchema, UsuariosSchema
from sqlalchemy.orm import Session
from models import Produtos, User

#cria a tabela
Base.metadata.create_all(bind=engine)
router = APIRouter(prefix="/usuarios")   

def get_db():
    try:
        db = SessionLocal()
        #TODO 
        yield db
    finally:
        db.close()




@router.post("/add")
async def add_usuario(request:UsuariosSchema, db: Session = Depends(get_db)):
    usuario_on_db = User(
        id=request.id
        , username=request.username
        , password=request.password
        , email=request.email
        , age=request.age
        , recover_email=recover_email
    )
    db.add(usuario_on_db)
    db.commit()
    db.refresh(usuario_on_db)
    return usuario_on_db

@router.get("/{username}", description="Listar os usuarios pelo nome")
def get_usuarios(username,db: Session = Depends(get_db)):
    usuario_on_db = db.query(User).filter(User.username == username).first()
    return usuario_on_db

@router.delete("/{id}", description="Deletar o Usuario pelo id")
def delete_usuario(id: int, db: Session = Depends(get_db)):
    usuario_on_db = db.query(User).filter(User.id == id).first()
    if usuario_on_db is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Usuario com este id não encontrado!')
    db.delete(usuario_on_db)
    db.commit()
    return f"Usuario com o id: {id} deletado com sucesso!", Response(status_code=status.HTTP_200_OK)

# @app.put("{id}",response_model=User)
# async def update_usuario(request:UsuariosSchema, id: int, db: Session = Depends(get_db)):
#     usuario_on_db = db.query(User).filter(User.id == id).first()
#     print(usuario_on_db)
#     if usuario_on_db is None:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Usuario com este id não encontrado!')
#     usuario_on_db = User(id=request.id, item=request.item, peso=request.peso, numero_caixas=request.numero_caixas)
#     db.up
#     db.(usuario_on_db)
#     db.commit()
#     db.refresh(usuario_on_db)
#     return usuario_on_db, Response(status_code=status.HTTP_204_NO_CONTENT)


# router = APIRouter()
