from pydantic import BaseModel, EmailStr

class ProdutosSchema(BaseModel):
    id: int
    item: str
    peso: float
    numero_caixas: int

class UsuariosSchema(BaseModel):
    id: int
    username: str
    password: str
    email: EmailStr
    age: int
    recover_email: str