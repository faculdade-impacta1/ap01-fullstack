from sqlalchemy import Column, Integer, SmallInteger, String, DateTime, Float
from database import Base
from sqlalchemy.sql import func
#cria o a tabela tarefas



class Produtos(Base):
    __tablename__ = "produtos"

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    item = Column('item', String, nullable=False)
    peso = Column('peso', Float)
    numero_caixas = Column('numero_caixas', Integer)
    created_at = Column('created_at', DateTime, server_default=func.now())
    updated_at = Column('updated_at', DateTime, onupdate=func.now())

# Implementação AP01 Francisco folli
class User(Base):
    __tablename__ = 'users'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    username = Column('username', String, nullable=False, unique=True)
    password = Column('password', String, nullable=False)
    email = Column('email', String(100), nullable=False)
    age = Column('age', SmallInteger, nullable=False)
    recover_email = Column('recover_email', String(100), nullable=False)
    created_at = Column('created_at', DateTime, server_default=func.now())
    updated_at = Column('updated_at', DateTime, onupdate=func.now())
